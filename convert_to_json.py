import os
import sys
import pandas
import csv
import json
sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)
basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"JSON_CDM")
if not os.path.exists(destination):
    os.mkdir(destination)
else:
    for root, dirs, files in os.walk(destination, topdown=False):
            for name in files:
                os.remove(os.path.join(root, name))
            for name in dirs:
                os.rmdir(os.path.join(root, name))
    if  os.path.exists(destination):
         os.rmdir(destination)
    os.mkdir(destination)

source=os.path.join(basePath,"JSON_DATA")
for folder in os.listdir(source):
    moveToState=os.path.join(source,folder)
    checkDestination=os.path.join(destination,folder)
    if not os.path.exists(checkDestination):
        os.mkdir(checkDestination)

    for files in  os.listdir(moveToState):

          with open(os.path.join(moveToState,files)) as f:
              reader = csv.DictReader(f)
              rows = list(reader)
              files=files.replace('.csv','')

          destinationToSave = os.path.join(checkDestination, files+'.json')
          with open(destinationToSave, 'w') as f:
              json.dump(rows, f)

destination=os.path.join(basePath,"JSON_DATA")
for root, dirs, files in os.walk(destination, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))
if  os.path.exists(destination):
     os.rmdir(destination)