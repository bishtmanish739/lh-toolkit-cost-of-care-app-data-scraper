import os
import sys
import pandas
sys.path.append(os.getcwd())

from ParseData import ParseData
a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)
basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"CDM")
if not os.path.exists(destination):
    os.mkdir(destination)
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Medicare Data'),"Inpatient")
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)

filePath=os.path.join(here,"rows.csv")
content = pandas.read_csv(filePath,usecols=['DRG_DESC', 'FACILITY_NAME','STATE_DESC','MEAN_MEDICARE_PAYMENTS'],encoding= 'unicode_escape')
content.rename(columns={'FACILITY_NAME':'Name','MEAN_MEDICARE_PAYMENTS': 'Charge', 'DRG_DESC': 'Description','STATE_DESC':'State'}, inplace=True)
content['Category']='Standard'
content['Description']=content['Description'].str.split('-').str[1]
content = content.dropna(how='all')
#Reading Outpateint file
here=os.path.join(os.path.join(pathToData,'Medicare Data'),"Outpatient")
filePath=os.path.join(here,"rows.csv")
out = pandas.read_csv(filePath,usecols=['APC_Desc', 'Provider_Name','Provider_State','Average_Medicare_Payment_Amount'],encoding= 'unicode_escape')
out.rename(columns={'Provider_Name':'Name','Average_Medicare_Payment_Amount': 'Charge', 'APC_Desc': 'Description','Provider_State':'State'}, inplace=True)
out['Category']='DRG'
out = out.dropna(how='all')
print(content.shape)
print(out.shape)
content = pandas.concat([content, out])
content["Name"] = content["Name"].str.upper()
out["Name"] = out["Name"].str.upper()
content.sort_values(by=['Name'] ,  inplace = True,axis = 0,ascending = True)
print(content.shape)
content = content.reset_index(drop=True)

us_states = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}
prevV=""
prevI=0
prevState=''
nrow=content.shape[0] -1
prev=0
for index,row in content.iterrows():
       if prevV!=row["Name"]:
           if (prevI != 0):
                 percent = int((int(index) / int(nrow)) * 100)
                 if (percent >prev):
                  print(percent,'%')
                  prev=percent
                 prevV=prevV.replace('/', '')
                 stateName=us_states[prevState]
                 state=os.path.join(destination,stateName)
                 if stateName!="Alaska" and stateName !="New York" and stateName!="Indiana":
                     stateFolder=os.path.join(destination,state)
                     if not os.path.exists(stateFolder):
                         os.mkdir(stateFolder)
                     check = os.path.join(stateFolder, prevV.title() + ".csv")
                     if os.path.exists(check):
                          os.remove(check)
                     destinationToSave = os.path.join(state, prevV.title() + ".csv")
                     df=content[prevI:index]
                     df=df.drop(columns =['State','Name'])
                     df.to_csv(destinationToSave, mode='a', encoding='utf-8', index=False)
           prevV = row["Name"]
           prevI = index
           prevState=row['State']
       if index == nrow:
            percent = int((int(index) / int(nrow)) * 100)
            if (percent > prev):
                print(percent,'%')
                prev = percent

            stateName = us_states[row["State"]]
            state = os.path.join(destination, stateName)
            if stateName != "Alaska" and stateName != "New York"  and stateName != "Indiana":
                stateFolder = os.path.join(destination, state)
                if not os.path.exists(stateFolder):
                    os.mkdir(stateFolder)
                check = os.path.join(stateFolder, prevV.title() + ".csv")
                if os.path.exists(check):
                    os.remove(check)
                destinationToSave = os.path.join(state, prevV.title() + ".csv")
                df = content[prevI:index]
                df = df.drop(columns=['State', 'Name'])
                df.to_csv(destinationToSave, mode='a', encoding='utf-8', index=False)
