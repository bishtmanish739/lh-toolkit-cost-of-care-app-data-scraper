import importlib
import os
import sys
import json

sys.path.append(os.getcwd())

from ParseData import ParseData
import pandas as pd

a = ParseData()
here = os.path.dirname(os.path.abspath(__file__))
folder = os.path.basename(here)

basePath=here.split("process_cdm")[0]
destination=os.path.join(basePath,"CDM")
pathToData=os.path.join(basePath,'Data')
here=os.path.join(os.path.join(pathToData,'Alaska'),folder)
if not os.path.exists(here):
    print("No Data Found For {0}".format(here))
    exit(0)
if not os.path.exists(destination):
    os.mkdir(destination)
print(destination)
state = os.path.join(destination, 'Alaska')  # Storing Statewise
if not os.path.exists(state):
    os.mkdir(state)
destination = os.path.join(state, folder+".csv")
if os.path.exists(destination):
    os.remove(destination)     #Removing from CDM
results_json = os.path.join(here, 'records.json')
with open(results_json, 'r') as filey:
    results = json.loads(filey.read())
for result in results:

    filename = os.path.join(here, result['filename'])
    name=result['name']
    hospitalId='none'
    if not os.path.exists(filename):
        print('%s is not found in latest folder.' % filename)
        continue

    if os.stat(filename).st_size == 0:
        print('%s is empty, skipping.' % filename)
        continue
    charge=""
    description=""
    if 'drg' in filename.lower():
        n=8
        category = 'DRG'
        description = 'Description'
        charge='Average Proposed Charge'
        a.ProcessXLSX(n, filename,hospitalId, charge, description, category, destination)

    elif 'cms' in filename.lower():
       category='Standard'
       n=7
       print(destination)
       xls = pd.ExcelFile(filename)
       for sheet in xls.sheet_names:
        if filename.endswith('csv'):
         if 'procedure' in sheet.lower():
           description = 'CHARGE DESCRIPTION'
           charge ='AVERAGE UNIT PRICE (OUTPATIENT)'
           a.ProcessXLSX(n, filename, hospitalId,charge, description, category, destination, sheet)
        elif 'medication' in sheet.lower():
            category="Pharmacy"
            n=6
            description ='DRUG GENERIC NAME'
            charge =  'UNIT PRICE'
            a.ProcessXLSX(n, filename, hospitalId, charge,description, category, destination, sheet)
